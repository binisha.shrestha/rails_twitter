Rails.application.routes.draw do
  resources :tweets

  get '/tweets/:title' => 'tweets#search'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
